package com.dino.dinos

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import java.io.File

abstract class Dino(name: String) {
    val name = name
    val idle = animation("Idle")
    val walk = animation("Walk")
    val run = animation("Run")
    val dead = animation("Dead")
    val attack = animation("Attack")
    var state = 0.0f
    var currentAnimation = dead
    var xPos = 10f
    var yPos = 10f

    var vol = Vector2(0f, -1f)



    fun animation(type: String): Animation<TextureRegion> {
        val l = mutableListOf<TextureRegion>()
        val path = "dinos/png/$name"

        val size = File(path)
                .listFiles { _: File, name: String -> name.contains(type) }
                ?.size ?: 0

        for (x in 1..size) {
            val t = Texture("$path/$type ($x).png")
            l.add(TextureRegion(t))
        }

        val a = Animation(0.08f, *l.toTypedArray())

        a.playMode = Animation.PlayMode.LOOP

        return a
    }
}